### Структура таблицы user:

| Колонка                                                                                        | Тип                                            |
|------------------------------------------------------------------------------------------------|------------------------------------------------|
| id                                                                                             | int(11) NOT NULL AUTO_INCREMENT                |
| external_id                                                                                    | varchar(255) NOT NULL                          |
| title                                                                                          | varchar(255) NOT NULL                          |
| text                                                                                           | varchar(255) NOT NULL                          |
| creation_date                                                                                  | datetime NOT NULL DEFAULT    CURRENT_TIMESTAMP |
| fields                                                                                         | varchar(255) DEFAULT NULL                      |
| PRIMARY KEY (`id`),<br/>UNIQUE KEY (`external_id`),<br/>ENGINE=MyISAM DEFAULT<br/>CHARSET=utf8 |                                                |
<br/>
Поле id - внутренний автоинкрементируемый id.<br/>
Поле external_id - идентификатор во внешней системе, из которой мы получили информацию о записе.