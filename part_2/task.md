### Задача:

Есть инструмент аудита сайта, который через cURL (с кастомными заголовками) делает запросы к страницам сайта.<br/><br/>
Имеется сайт, запросы к которому с сервера всегда выдают ошибку "cURL error 28: Connection timed out".<br/><br/>
Необходимо описать алгоритм действий в установлении причин ошибки и её исправления. <br/><br/><br/>

### Решение:

Ошибка выдается если долго не удалось получать данные от сайта.

#### Выясняем причину ошибки:
1. Проверяем что сам сайт доступен (через тот же бразуер)
2. Проверяем доступность сайта из раных точек (страны, мира) с помощью сторонних сервисов. Например https://ping-admin.ru/free_test/
3. Если на сайте установлены защита, на сервере сайта или внешняя, то проверить не блочат запросы они. У меня так любил делать cloudflare или когда был reverse proxy сайт.
4. Если есть доступ к серверу можно проверить по nginx|apache логам проверить приходил запрос или нет
5. Возможно запрос вызывается слишком часто
6. Попробовать повторить запрос к сайту. С заголовками и без, с сервера и локально (postman, curl через консоль или другие методы)

Каждый этап ведет к разным возможностям исправления ошибки.<br/><br/>
Чаще всего в таких случая проблема на стороне сайта: упал, повис, защита сработала и т.д.
И обычно проблема решается на стороне сайта, хостинга.<br/> <br/>
Могла где то посередине сдонуть линия. Тут только ждать<br/><br/>
С нашей стороны мы можем проверить время ожидания ответа. Возможно оно слишком маленькое.


Мне немного нехватило конкретики в данном задании. Ошибки могут быть разными и разные способы решения.