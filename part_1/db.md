### Структура таблицы user:

| Колонка                                                                                                       | Тип                                            |
|---------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| id                                                                                                            | int(11) NOT NULL AUTO_INCREMENT                |
| email                                                                                                         | varchar(255) NOT NULL                          |
| name                                                                                                          | varchar(255) NOT NULL                          |
| password                                                                                                      | varchar(255) NOT NULL                          |
| creation_date                                                                                                 | datetime NOT NULL DEFAULT    CURRENT_TIMESTAMP |
| PRIMARY KEY (`id`),<br/>UNIQUE KEY (`email`),<br/>KEY (`creation_date`)<br/>ENGINE=MyISAM DEFAULT<br/>CHARSET=utf8 |                                                |

### Параметры ДБ
    
SHOW VARIABLES;<br/>
long_query_time 10.000000<br/>
low_priority_updates OFF<br/>
lower_case_file_system ON<br/>
lower_case_table_names 2<br/>
max_allowed_packet 104857600<br/>
max_binlog_cache_size 18446744073709547520<br/>
max_binlog_size 1073741824<br/>
max_binlog_stmt_cache_size 18446744073709547520<br/>
max_connect_errors 10<br/>
max_connections 151<br/>
max_delayed_threads 20<br/>
max_error_count 64<br/>
max_heap_table_size 16777216<br/>
max_insert_delayed_threads 20<br/>
max_join_size 18446744073709551615<br/>
max_length_for_sort_data 1024<br/>
max_long_data_size 1048576<br/>
max_prepared_stmt_count 16382<br/>
max_relay_log_size 0<br/>
max_seeks_for_key 18446744073709551615<br/>
max_sort_length 1024<br/>
max_sp_recursion_depth 0<br/>
max_tmp_tables 32<br/>
max_user_connections 0<br/>
max_write_lock_count 18446744073709551615<br/>