<?php

class Part_1
{
    /**
     * @param int[] $ids
     * @return array
     */

    public function loadUsersByIds(array $ids): array
    {
        # Прервыаем выполнение функции, если нет ids
        # Нет смысла посылать запрос в бд с фильтром по id = 0
        if (count($ids) === 0) return [];

        # Если у нас есть ограничение в БД на длину запроса, количество строк или что то подобное, то можно
        # вызвать запросы в цикле разбив на чанки.
        $idsFilter = $this->_getInCondition('id', $ids);

        return $this->_loadObjectsByFilter('user', [$idsFilter]);
    }

    private function _getInCondition(string $field, array $values): string
    {
        # Перенес проверку из этого метода

        # $uniqueValues не использовался
        $uniqueValues = array_unique(array_filter($values));
        $quotedValues = array_map(fn(string $value) => $this->_quoteString($uniqueValues));
        $implodedValues = implode(', ', $quotedValues);
        $quotedField = "`{$field}`";

        # Убрал 'AND {$quotedField} IS NOT)', т.к. не понял что тут было изначально.
        # Скорее всего побилось из за переноса через word
        return "({$quotedField} IN ({$implodedValues})";
    }

    private function _quoteString(string $value): string
    {
        $conn = new PDO('');
        return $conn->quote($value);
    }

    private function _loadObjectsByFilter(string $objectName, array $filter = []): array
    {
        #
        //some PDO work, result query will be SELECT * FROM $objectName WHERE ($f)
        return [];
    }

}