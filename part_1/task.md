### Задача:

Есть код, который загружает пользователей из базы данных (MySQL) по id, далее с ними выполняются какие-то действия. Количество id, которые могут быть переданы, от одного до нескольких миллионов.<br/><br/>
При загрузке из базы данных мы периодически получаем ошибку “MySQL query error”.<br/><br/>
Ниже представлен код метода, структура таблицы в базе данных и информация по конфигурации базы данных.<br/><br/>
Необходимо предложить решение, которое позволит избежать ошибок при работе кода. Решение может быть представлено как исправлением кода, так и исправление  структуры самой таблицы или конфигурации базы данных.<br/><br/> 

### Решение:

Из за большого объма данных, как и на вход так и на выходе у нас падает выполнение запроса.<br>
#### Что можно сделать:
1. Увеличить объем пересылаемых пакетов max_allowed_packet
2. Разбить запрос на несколько, с уменьшением его длинны
3. Снизить количество выбираеммых данных из одной строки, указать конкретные выбираемы поля для select 
4. Решение через изменение БД не нашел =(